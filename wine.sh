#! /bin/bash
sudo dpkg --add-architecture i386 
wget -nc https://dl.winehq.org/wine-builds/winehq.key
sudo mv winehq.key /usr/share/keyrings/winehq-archive.key
touch $(lsb_release -r) && rm Release\:
if [ -f 22.04 ]; then
rm 22.04
wget -nc https://dl.winehq.org/wine-builds/ubuntu/dists/jammy/winehq-jammy.sources
sudo mv winehq-jammy.sources /etc/apt/sources.list.d/
fi
if [ -f 21.10 ]; then
rm 21.10
wget -nc https://dl.winehq.org/wine-builds/ubuntu/dists/impish/winehq-impish.sources
sudo mv winehq-impish.sources /etc/apt/sources.list.d/
fi
if [ -f 20.04 ]; then
rm 20.04
wget -nc https://dl.winehq.org/wine-builds/ubuntu/dists/focal/winehq-focal.sources
sudo mv winehq-focal.sources /etc/apt/sources.list.d/
fi
if [ -f 18.04 ]; then
rm 18.04
wget -nc https://dl.winehq.org/wine-builds/ubuntu/dists/bionic/winehq-bionic.sources
sudo mv winehq-bionic.sources /etc/apt/sources.list.d/
fi
sudo apt update
sudo apt install --install-recommends wine-stable -y
